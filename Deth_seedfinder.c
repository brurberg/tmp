// check the biome at a block position
#include "finders.h"
#include "math.h"
#include "util.h"
#include "generator.h"
#include <stdio.h>

float min_major_biomes = 0; //minimum major biome percent

struct compactinfo_t
{
	int64_t seedStart, seedEnd;
	int thread_id;
};

#ifdef USE_PTHREAD
static void *searchCompactBiomesThread(void *data)
#else
static DWORD WINAPI searchCompactBiomesThread(LPVOID data)
#endif
{
	struct compactinfo_t info_new = *(struct compactinfo_t *)data;
  // Set up a biome generator that reflects the biome generation of
  // Minecraft 1.18.
  Generator g;
  int mc = MC_1_18;
  setupGenerator(&g, mc, 0);
	enum BiomeID biomes[] = {ice_spikes, bamboo_jungle, desert, plains, ocean, jungle, forest, mushroom_fields, mesa, flower_forest, warm_ocean, frozen_ocean, megaTaiga, roofedForest, extremeHills, swamp, savanna, icePlains};
  BiomeFilter filter = setupBiomeFilter(biomes,
						  sizeof(biomes) / sizeof(enum BiomeID), NULL, 0);
  Range r;
  // 1:16, a.k.a. horizontal chunk scaling
  r.scale = 16;
  // Define the position and size for a horizontal area:
  r.x = -128, r.z = -128;   // position (x,z)
  r.sx = 256, r.sz = 256; // size (width,height)
  // Set the vertical range as a plane near sea level at scale 1:4.
  r.y = 15, r.sy = 1;
  //Range r = {4, -512, -512, 1024, 1024, 15, 1};//-2048, -2048, 4096, 4096, 64, 1};
  //Range r = {4, -512, -512, 1024, 1024, 15, 1};//-2048, -2048, 4096, 4096, 64, 1};
  int *cache = allocCache(&g, r);

  // Seeds are internally represented as unsigned 64-bit integers.
  uint64_t seed;
  for (seed = info_new.seedStart; seed < info_new.seedEnd; seed++)
  {
    // Apply the seed to the generator for the Overworld dismension.
    applySeed(&g, 0, seed);
    printf("Seed: %" PRId64 "\n", (int64_t) seed);
	  if (!checkForBiomes(&g, cache, r, 0, seed, filter, 0, 0)) {
      if (seed >= 20) {
        //return 0;
      }
  		continue;
    }
    printf("SEED had all biomes\n");

		Pos goodhuts[2];
		int hut_count = 0;
		Pos huts[100];
		int huts_found = 0;
		int rad = r.sx / SWAMP_HUT_CONFIG.regionSize / r.scale / 2;
    int z, x;
		for (z = -rad; z < rad; z++)
		{
			for (x = -rad; x < rad; x++)
			{
				Pos p;
				if (!getStructurePos(Swamp_Hut, mc, seed, x, z, &p)) {
          continue;
        }
				if (isViableStructurePos(Swamp_Hut, &g, p.x, p.z, 0))
				{
					if (abs(p.x) < r.sx && abs(p.z) < r.sx)
					{
						huts[hut_count] = p;
						hut_count++;
						for (int i = 0; i < hut_count; i++)
						{
							for (int j = 0; j < hut_count; j++)
							{
								if (j == i)
									continue;
								float dx, dz;
								dx = abs(huts[i].x - huts[j].x);
								dz = abs(huts[i].z - huts[j].z);
								if (sqrt((dx * dx) + (dz * dz)) <= 200)
								{
									goodhuts[0] = huts[i];
									goodhuts[1] = huts[j];
									huts_found = 1;
								}
							}
						}
					}
				}
			}
	  }
		if (!huts_found)
			continue;
    printf("SEED had huts\n");

		int monument_count = 0;
		rad = r.sx / MONUMENT_CONFIG.regionSize / r.scale / 2;
		for (z = -rad; z < rad; z++)
		{
			for (x = -rad; x < rad; x++)
			{
				Pos p;
				if (!getStructurePos(Monument, mc, seed, x, z, &p)) {
          continue;
        }
				if (isViableStructurePos(Monument, &g, p.x, p.z, 0))
					if (abs(p.x) < r.sx && abs(p.z) < rad)
						monument_count++;
			}
		}
		if (monument_count == 0)
			continue;

		int ocean_count = 0;
		// biome enum defined in layers.h
		enum BiomeID req_biomes[] = {ice_spikes, bamboo_jungle, desert, plains, ocean, jungle, forest, mushroom_fields, mesa, flower_forest, warm_ocean, frozen_ocean, megaTaiga, roofedForest, extremeHills, swamp, savanna, icePlains};
		int biome_exists[sizeof(req_biomes) / sizeof(enum BiomeID)] = {0};
		enum BiomeID major_biome_percent[11][16] = {
			{desert, desert_lakes, desert_hills},
			{plains, sunflower_plains},
			{extremeHills, mountains, wooded_mountains, gravelly_mountains, modified_gravelly_mountains, mountain_edge},
			{jungle, jungle_hills, modified_jungle, jungle_edge, modified_jungle_edge, bamboo_jungle, bamboo_jungle_hills},
			{forest, wooded_hills, flower_forest, birch_forest, birch_forest_hills, tall_birch_forest, tall_birch_hills},
			{roofedForest, dark_forest, dark_forest_hills},
			{badlands, badlands_plateau, modified_badlands_plateau, wooded_badlands_plateau, modified_wooded_badlands_plateau, eroded_badlands},
			{swamp, swamp_hills},
			{savanna, savanna_plateau, shattered_savanna, shattered_savanna_plateau},
			{ice_spikes, snowy_beach, snowy_mountains, snowy_taiga, snowy_taiga_hills, snowy_taiga_mountains, snowy_tundra},
			{taiga, taiga_hills, taiga_mountains, snowy_taiga, snowy_taiga_hills, snowy_taiga_mountains, giant_tree_taiga, giant_tree_taiga_hills, giant_spruce_taiga, giant_spruce_taiga_hills}};
		char *major_biome_percent_string[11] = {"desert", "plains", "hills & mountains", "jungle", "forest", "roofed forest", "mesa", "swamp", "savanna", "ice & snow", "taiga"};
		int major_biome_percent_counter[11] = {0};
		enum BiomeID biome_percent[] = {badlands, badlands_plateau, bamboo_jungle, bamboo_jungle_hills, basalt_deltas, beach, birch_forest, birch_forest_hills, cold_ocean, crimson_forest, dark_forest, dark_forest_hills, deep_cold_ocean, deep_frozen_ocean, deep_lukewarm_ocean, deep_ocean, deep_warm_ocean, desert, desert_hills, desert_lakes, end_barrens, end_highlands, end_midlands, eroded_badlands, flower_forest, forest, frozen_ocean, frozen_river, giant_spruce_taiga, giant_spruce_taiga_hills, giant_tree_taiga, giant_tree_taiga_hills, gravelly_mountains, ice_spikes, jungle, jungle_edge, jungle_hills, lukewarm_ocean, modified_badlands_plateau, modified_gravelly_mountains, modified_jungle, modified_jungle_edge, modified_wooded_badlands_plateau, mountain_edge, mountains, mushroom_fields, mushroom_field_shore, nether_wastes, ocean, plains, river, savanna, savanna_plateau, shattered_savanna, shattered_savanna_plateau, small_end_islands, snowy_beach, snowy_mountains, snowy_taiga, snowy_taiga_hills, snowy_taiga_mountains, snowy_tundra, soul_sand_valley, stone_shore, sunflower_plains, swamp, swamp_hills, taiga, taiga_hills, taiga_mountains, tall_birch_forest, tall_birch_hills, the_end, the_void, warm_ocean, warped_forest, wooded_badlands_plateau, wooded_hills, wooded_mountains};
		char *biome_percent_string[sizeof(biome_percent) / sizeof(enum BiomeID)] = {"badlands", "badlands_plateau", "bamboo_jungle", "bamboo_jungle_hills", "basalt_deltas", "beach", "birch_forest", "birch_forest_hills", "cold_ocean", "crimson_forest", "dark_forest", "dark_forest_hills", "deep_cold_ocean", "deep_frozen_ocean", "deep_lukewarm_ocean", "deep_ocean", "deep_warm_ocean", "desert", "desert_hills", "desert_lakes", "end_barrens", "end_highlands", "end_midlands", "eroded_badlands", "flower_forest", "forest", "frozen_ocean", "frozen_river", "giant_spruce_taiga", "giant_spruce_taiga_hills", "giant_tree_taiga", "giant_tree_taiga_hills", "gravelly_mountains", "ice_spikes", "jungle", "jungle_edge", "jungle_hills", "lukewarm_ocean", "modified_badlands_plateau", "modified_gravelly_mountains", "modified_jungle", "modified_jungle_edge", "modified_wooded_badlands_plateau", "mountain_edge", "mountains", "mushroom_fields", "mushroom_field_shore", "nether_wastes", "ocean", "plains", "river", "savanna", "savanna_plateau", "shattered_savanna", "shattered_savanna_plateau", "small_end_islands", "snowy_beach", "snowy_mountains", "snowy_taiga", "snowy_taiga_hills", "snowy_taiga_mountains", "snowy_tundra", "soul_sand_valley", "stone_shore", "sunflower_plains", "swamp", "swamp_hills", "taiga", "taiga_hills", "taiga_mountains", "tall_birch_forest", "tall_birch_hills", "the_end", "the_void", "warm_ocean", "warped_forest", "wooded_badlands_plateau", "wooded_hills", "wooded_mountains"};
		int biome_percent_counter[sizeof(biome_percent) / sizeof(enum BiomeID)] = {0};

    int step = 1;
    rad = r.sx / 2;
		for (z = -rad; z < rad; z += step)
		{
			for (x = -rad; x < rad; x += step)
			{
				int biome = getBiomeAt(&g, r.scale, x, r.y, z);
				if (isOceanic(biome))
					ocean_count++;
				if (abs(x) < rad && abs(z) < rad)
					for (int i = 0; i < sizeof(biome_exists) / sizeof(int); i++)
						if (biome == req_biomes[i])
							biome_exists[i] = -1;
				if (!isOceanic(biome))
					for (int i = 0; i < sizeof(major_biome_percent_counter) / sizeof(int); i++)
						for (int j = 0; j < sizeof(major_biome_percent[i]) / sizeof(enum BiomeID); j++)
							if (biome == major_biome_percent[i][j])
								major_biome_percent_counter[i]++;
				for (int i = 0; i < sizeof(biome_percent) / sizeof(enum BiomeID); i++) {
					if (biome == biome_percent[i]) {
						biome_percent_counter[i]++;
          }
        }
			}
		}

		//check for max ocean percent
		float ocean_percent = (ocean_count * (step * step) / (r.sx * r.sz)) * 100;
		//if (ocean_percent > max_ocean)
		//	continue;

		//check for minimum major biome percent
		int major_biome_less_than_min = 1;
		for (int i = 0; i < sizeof(major_biome_percent_counter) / sizeof(int); i++) {
			if ((major_biome_percent_counter[i] * (step * step) / (r.sx * r.sz)) * 100 < min_major_biomes) {
				major_biome_less_than_min = 0;
      }
    }
		if (!major_biome_less_than_min)
			continue;

		//verify all biomes are present
		int all_biomes = 1;
		for (int i = 0; i < sizeof(req_biomes) / sizeof(enum BiomeID); i++) {
			if (biome_exists[i] != -1) {
				all_biomes = 0;
      }
    }
		if (!all_biomes)
			continue;

		//get spawn biome
		Pos spawn = getSpawn(&g);
		int spawn_biome = getBiomeAt(&g, r.scale, spawn.x, r.y, spawn.z);
		char *spawn_biome_string;
		for (int i = 0; i < sizeof(biome_percent) / sizeof(enum BiomeID); i++)
			if (spawn_biome == biome_percent[i])
				spawn_biome_string = biome_percent_string[i];

		//get closest village
		Pos villages[500] = {0};
		int village_count = 0;
		rad = r.sx / VILLAGE_CONFIG.regionSize / 2;
		for (z = -rad; z < rad; z++)
		{
			for (x = -rad; x < rad; x++)
			{
				Pos p;
				if (!getStructurePos(Village, mc, seed, x, z, &p)) {
          continue;
        }
				if (isViableStructurePos(Village, &g, p.x, p.z, 0))
					if (abs(p.x) < rad && abs(p.z) < rad)
						villages[village_count++] = p;
			}
		}

		Pos closest_village;
		int closest_village_distance = -1;
		for (int i = 0; i < sizeof(villages) / sizeof(villages[0]); i++)
		{
			if (villages[i].x == 0 && villages[i].z == 0)
				break;
			float dx, dz;
			dx = abs(spawn.x - villages[i].x);
			dz = abs(spawn.z - villages[i].z);
			if (sqrt((dx * dx) + (dz * dz)) < closest_village_distance || closest_village_distance == -1)
			{
				closest_village = villages[i];
				closest_village_distance = sqrt((dx * dx) + (dz * dz));
			}
		}

		char out[512];
		snprintf(out, 512, "%" PRId64, seed);
		snprintf(out + strlen(out), 512 - strlen(out), ",%i", hut_count);
		snprintf(out + strlen(out), 512 - strlen(out), ",%i", monument_count);
		for (int i = 0; i < sizeof(biome_percent_counter) / sizeof(int); i++)
			snprintf(out + strlen(out), 512 - strlen(out), ",%.2f%%", (biome_percent_counter[i] * (step * step) / (r.sx * r.sz)) * 100);
		snprintf(out + strlen(out), 512 - strlen(out), "\n");

		/*if (raw)
		{
			printf("%s", out);
			fflush(stdout);
		}
		else
		{*/
			char info_out[512];
			snprintf(info_out, 512, "\n%17s: %" PRId64, "Found", seed);
			snprintf(info_out + strlen(info_out), 512 - strlen(info_out), "\n%17s: %s", "Spawn biome", spawn_biome_string);
			snprintf(info_out + strlen(info_out), 512 - strlen(info_out), "\n%17s: %i,%i", "Closest village", closest_village.x, closest_village.z);
			snprintf(info_out + strlen(info_out), 512 - strlen(info_out), "\n%17s: %i,%i & %i,%i", "Huts", goodhuts[0].x, goodhuts[0].z, goodhuts[1].x, goodhuts[1].z);
			snprintf(info_out + strlen(info_out), 512 - strlen(info_out), "\n%17s: %5.2f%%", "Ocean", ocean_percent);
			for (int i = 0; i < sizeof(major_biome_percent_counter) / sizeof(int); i++)
				snprintf(info_out + strlen(info_out), 512 - strlen(info_out), "\n%17s: %5.2f%%", major_biome_percent_string[i], (major_biome_percent_counter[i] * (step * step) / (r.sx * r.sz)) * 100);
			snprintf(info_out + strlen(info_out), 512 - strlen(info_out), "\n");
			fprintf(stderr, "\r%*c", 128, ' ');
			fflush(stdout);
			printf("%s", info_out);
			fflush(stdout);
		//}

		FILE *fp = fopen("found.csv", "r");
		if (fp == NULL)
		{
			fp = fopen("found.csv", "a");
			fprintf(fp, "seed");
			fprintf(fp, ",huts");
			fprintf(fp, ",monuments");
			for (int i = 0; i < sizeof(biome_percent_counter) / sizeof(int); i++)
				fprintf(fp, ",%s", biome_percent_string[i]);
			fprintf(fp, "\n");
		}
		else
		{
			fclose(fp);
			fp = fopen("found.csv", "a");
		}

		fprintf(fp, "%s", out);
		fclose(fp);
  }

	//freeGenerator(&g);
	free(cache);
#ifdef USE_PTHREAD
	pthread_exit(NULL);
#endif
	return 0;
}

int main(int argc, char *argv[])
{
	int64_t seedStart, seedEnd;
	unsigned int threads, t, range, fullrange;
	char genimage;
	BiomeFilter filter;
	int withHut, withMonument;
	int minscale;

	// arguments
	if (argc <= 0)
	{
		printf("find_compactbiomes [seed_start] [seed_end] [threads] [range]\n"
			   "\n"
			   "  seed_start    starting seed for search [long, default=0]\n"
			   "  end_start     end seed for search [long, default=-1]\n"
			   "  threads       number of threads to use [uint, default=1]\n"
			   "  range         search range (in blocks) [uint, default=1024]\n");
		exit(1);
	}
	if (argc <= 1 || sscanf(argv[1], "%" PRId64, &seedStart) != 1)
	{
		printf("Seed start: ");
		if (!scanf("%" SCNd64, &seedStart))
		{
			printf("That's not right");
			exit(1);
		}
	}
	if (argc <= 2 || sscanf(argv[2], "%" PRId64, &seedEnd) != 1)
	{
		printf("Seed end: ");
		if (!scanf("%" SCNd64, &seedEnd))
		{
			printf("That's not right");
			exit(1);
		}
	}
	if (argc <= 3 || sscanf(argv[3], "%u", &threads) != 1)
	{
		printf("Threads: ");
		if (!scanf("%i", &threads))
		{
			printf("That's not right");
			exit(1);
		}
	}

  int64_t total_seeds = 0;
	time_t start_time = time(NULL);

	thread_id_t threadID[threads];
	struct compactinfo_t info[threads];

	// store thread information
	if (seedStart == 0 && seedEnd == -1)
	{
		seedStart = -999999999999999999;
		seedEnd = 999999999999999999;
	}
	total_seeds = (uint64_t)seedEnd - (uint64_t)seedStart;
	uint64_t seedCnt = ((uint64_t)seedEnd - (uint64_t)seedStart) / threads;
	for (t = 0; t < threads; t++)
	{
		info[t].seedStart = (int64_t)(seedStart + seedCnt * t);
		info[t].seedEnd = (int64_t)(seedStart + seedCnt * (t + 1));
		info[t].thread_id = t;
	}
	info[threads - 1].seedEnd = seedEnd;

#ifdef USE_PTHREAD
	for (t = 0; t < threads; t++)
	{
		pthread_create(&threadID[t], NULL, searchCompactBiomesThread, (void *)&info[t]);
	}

	for (t = 0; t < threads; t++)
	{
		pthread_join(threadID[t], NULL);
	}

#else

	for (t = 0; t < threads; t++)
	{
		threadID[t] = CreateThread(NULL, 0, searchCompactBiomesThread, (LPVOID)&info[t], 0, NULL);
	}

	WaitForMultipleObjects(threads, threadID, TRUE, INFINITE);

#endif

	fprintf(stderr, "\r%*c", 128, ' ');
	char time_end[20];
	time_t end_time = time(NULL);
	strftime(time_end, 20, "%m/%d/%Y %H:%M:%S", localtime(&end_time));
	printf("\n%20s: %s\n", "Ended", time_end);
	printf("%20s: %ld seconds\n", "Total time elapsed", end_time - start_time);

	printf("\n\nPress [ENTER] to exit\n");
	fflush(stdout);
	getchar();
	return 0;
}
